const gulp = require('gulp');
const sass = require('gulp-dart-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const watch = require('gulp-watch');
const cssmin = require('gulp-cssmin');
const dependents = require('gulp-dependents');
const debug = require('gulp-debug'); // Add this

// styles only for admin pages
gulp.task( 'admin-styles', () => {
    return gulp.src('./assets/css/style.scss', { since: gulp.lastRun( 'admin-styles' ) })
        .pipe(dependents())
        .pipe(debug({title: 'dependents:'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssmin())
        .pipe(gulp.dest('./assets/css/'))
        .pipe(browserSync.stream());
});

gulp.task( 'watch', function () {
    watch('./assets/css/style.scss', gulp.series( 'admin-styles' ));
});

gulp.task( 'build', gulp.series( 'admin-styles' ));

gulp.task( 'default', gulp.parallel( gulp.series( 'admin-styles' ), 'watch' ));